// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (C) 2019 Daniel Borkmann <daniel@iogearbox.net> */

#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "l2md.h"

static void bootstrap_base(struct config *cfg)
{
	xmkdir1(cfg->general.base);
	xmkdir2(cfg->general.base, REPOS);
	xmkdir2(cfg->general.base, OIDS);
}

static void bootstrap_repos(struct config *cfg)
{
	struct config_repo *repo;
	struct config_url *url;
	struct stat sb = {};
	char path[PATH_MAX];
	uint32_t i, j;
	int ret;

	verbose("Initial repository check.\n");

	repo_for_each(cfg, repo, i) {
		slprintf(path, sizeof(path), "%s/%s", REPOS, repo->name);
		xmkdir2(cfg->general.base, path);

		slprintf(path, sizeof(path), "%s/%s", OIDS, repo->name);
		xmkdir2(cfg->general.base, path);

		url_for_each(repo, url, j) {
			repo_local_path(cfg, repo, url, path, sizeof(path));
			ret = stat(path, &sb);
			if (ret)
				repo_clone(repo, url, path);
			else
				repo_pull(repo, url, path);
		}
	}

	verbose("Initial repository check completed.\n");
}

static void bootstrap_mail(struct config *cfg)
{
	cfg->ops->bootstrap(cfg);
}

void bootstrap_env(struct config *cfg)
{
	bootstrap_base(cfg);
	bootstrap_mail(cfg);
	bootstrap_repos(cfg);

	verbose("Bootstrap done.\n");
}

void sync_done(struct config *cfg)
{
	verbose("Sync done. ");

	if (!cfg->oneshot) {
		verbose("Sleeping %us.\n", cfg->general.period);
		sleep(cfg->general.period);
	}
}

void sync_env(struct config *cfg)
{
	struct config_repo *repo;
	struct config_url *url;
	char path[PATH_MAX];
	uint32_t i, j;

	verbose("Resyncing repositories.\n");

	repo_for_each(cfg, repo, i) {
		url_for_each(repo, url, j) {
			repo_local_path(cfg, repo, url, path, sizeof(path));
			repo_pull(repo, url, path);
		}
	}
}
