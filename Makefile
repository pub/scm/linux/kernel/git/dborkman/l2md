CFLAGS  = -O2 -Wall -Werror
CFLAGS += -Wextra -Wno-unused-parameter -Wno-missing-field-initializers
LDFLAGS = -lgit2
PREFIX ?= /usr/bin

l2md: l2md.o config.o env.o utils.o repo.o mail.o maildir.o pipe.o
	$(CC) -o $@ $^ $(LDFLAGS)

install:
	cp l2md $(PREFIX)/l2md

uninstall:
	$(RM) $(PREFIX)/l2md

clean:
	$(RM) *.o l2md
