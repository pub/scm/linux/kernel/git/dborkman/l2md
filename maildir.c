// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (C) 2019 Daniel Borkmann <daniel@iogearbox.net> */

#include <unistd.h>

#include <sys/time.h>
#include <sys/types.h>

#include "l2md.h"

static void maildir_new_mail(struct config_repo *repo, uint32_t which,
			     const char *oid, const void *raw, size_t len)
{
	char dst[PATH_MAX];

	slprintf(dst, sizeof(dst), "%s/new/0.%06u.%s-%u-%s",
		 repo->out, own_pid, repo->name, which, oid);

	xwrite_file(dst, raw, len, true);
}

static void maildir_bootstrap(struct config *cfg)
{
	struct config_repo *repo;
	uint32_t i;

	repo_for_each(cfg, repo, i) {
		xmkdir1_with_subdirs(repo->out);

		xmkdir2(repo->out, "cur");
		xmkdir2(repo->out, "tmp");
		xmkdir2(repo->out, "new");
	}
}

static void maildir_set_defaults(struct config *cfg)
{
	char path[PATH_MAX];

	slprintf(path, sizeof(path), "%s/maildir", cfg->general.base);
	__strlcpy(cfg->general.out, path, sizeof(cfg->general.out));
}

const struct mail_ops ops_maildir = {
	.name		= "maildir",
	.bootstrap	= maildir_bootstrap,
	.new_mail	= maildir_new_mail,
	.set_defaults	= maildir_set_defaults,
};
